<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210404152654 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE bonds ADD type INT NOT NULL, ADD ticker VARCHAR(6) NOT NULL, ADD isin VARCHAR(12) NOT NULL, ADD name VARCHAR(255) NOT NULL, ADD date_start DATETIME NOT NULL, ADD date_end DATETIME DEFAULT NULL, ADD base_currency VARCHAR(10) DEFAULT NULL, ADD lot_size INT NOT NULL');
        $this->addSql('ALTER TABLE promotion CHANGE type_promotion type_assets INT NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE bonds DROP type, DROP ticker, DROP isin, DROP name, DROP date_start, DROP date_end, DROP base_currency, DROP lot_size');
        $this->addSql('ALTER TABLE promotion CHANGE type_assets type_promotion INT NOT NULL');
    }
}
