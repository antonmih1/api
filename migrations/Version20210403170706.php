<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210403170706 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE exchange (id INT AUTO_INCREMENT NOT NULL, code INT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE issuer (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, country VARCHAR(255) NOT NULL, branch INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE assets ADD issuer_id INT NOT NULL, ADD exchange_id INT NOT NULL');
        $this->addSql('ALTER TABLE assets ADD CONSTRAINT FK_79D17D8EBB9D6FEE FOREIGN KEY (issuer_id) REFERENCES issuer (id)');
        $this->addSql('ALTER TABLE assets ADD CONSTRAINT FK_79D17D8E68AFD1A0 FOREIGN KEY (exchange_id) REFERENCES exchange (id)');
        $this->addSql('CREATE INDEX IDX_79D17D8EBB9D6FEE ON assets (issuer_id)');
        $this->addSql('CREATE INDEX IDX_79D17D8E68AFD1A0 ON assets (exchange_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE assets DROP FOREIGN KEY FK_79D17D8E68AFD1A0');
        $this->addSql('ALTER TABLE assets DROP FOREIGN KEY FK_79D17D8EBB9D6FEE');
        $this->addSql('DROP TABLE exchange');
        $this->addSql('DROP TABLE issuer');
        $this->addSql('DROP INDEX IDX_79D17D8EBB9D6FEE ON assets');
        $this->addSql('DROP INDEX IDX_79D17D8E68AFD1A0 ON assets');
        $this->addSql('ALTER TABLE assets DROP issuer_id, DROP exchange_id');
    }
}
