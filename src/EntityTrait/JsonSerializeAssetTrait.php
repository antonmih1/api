<?php

namespace App\EntityTrait;

/**
 * Trait class
 */
trait JsonSerializeAssetTrait
{
    /**
     * Specify data which should be serialized to JSON
     * @link https://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    public function jsonSerialize()
    {
        return [
            "id" => $this->getId(),
            "name" => $this->getName(),
            "type" => $this->getType(),
            "isin" => $this->getIsin(),
            "ticker" => $this->getTicker(),
            "code" => $this->getExchange()->getCode(),
            "issuer" => $this->getIssuer()->getName(),
        ];
    }
}