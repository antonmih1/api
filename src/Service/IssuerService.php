<?php

namespace App\Service;

use App\Entity\Issuer;
use App\Repository\BondRepository;
use App\Repository\PromotionRepository;

class IssuerService
{
    public function getIssuerId(PromotionRepository $promotionRepository, BondRepository $bondRepository, Issuer $issuer)
    {
        $promotions = $promotionRepository->getAssetsThisIssuer($issuer);
        $bonds = $bondRepository->getAssetsThisIssuer($issuer);
        if (!$bonds && !$promotions) {
            $data = [
                'status' => 404,
                'errors' => "Assets not found",
            ];
            return $data;
        }
        return array_merge($bonds, $promotions);
    }

}