<?php

namespace App\Service;

use App\Entity\Issuer;
use App\Repository\BondRepository;
use App\Repository\PromotionRepository;

class AssetService
{
    public function search(PromotionRepository $promotionRepository, BondRepository $bondRepository, $query, $type)
    {
        $assets_filters = [];
        $promotions = $promotionRepository->getAssets($query, $type);
        $bonds = $bondRepository->getAssets($query, $type);
        if (!$bonds && !$promotions) {
            $data = [
                'status' => 404,
                'errors' => "Assets not found",
            ];
            return $data;
        }
        $assets = array_merge($bonds, $promotions);
        foreach ($assets as $row) {
            $assets_filters[$row->getType()][] = $row;
        }

        return $assets_filters;
    }

}