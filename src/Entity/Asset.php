<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\HasLifecycleCallbacks()
 */
abstract class Asset
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\Column(type="integer")
     */
    protected $type;

    /**
     * @ORM\Column(type="string", length=6)
     */
    protected $ticker;

    /**
     * @ORM\Column(type="string", length=12)
     */
    protected $isin;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $name;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $date_start;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $date_end;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    protected $base_currency;

    /**
     * @ORM\Column(type="integer")
     */
    protected $lot_size;

    /**
     * @ORM\Column(type="integer")
     */
    protected $type_assets;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getType(): ?int
    {
        return $this->type;
    }

    public function setType(int $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getTicker(): ?string
    {
        return $this->ticker;
    }

    public function setTicker(string $ticker): self
    {
        $this->ticker = $ticker;

        return $this;
    }

    public function getIsin(): ?string
    {
        return $this->isin;
    }

    public function setIsin(string $isin): self
    {
        $this->isin = $isin;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDateStart(): ?\DateTimeInterface
    {
        return $this->date_start;
    }

    public function setDateStart(\DateTimeInterface $date_start): self
    {
        $this->date_start = $date_start;

        return $this;
    }

    public function getDateEnd(): ?\DateTimeInterface
    {
        return $this->date_end;
    }

    public function setDateEnd(?\DateTimeInterface $date_end): self
    {
        $this->date_end = $date_end;

        return $this;
    }

    public function getBaseCurrency(): ?string
    {
        return $this->base_currency;
    }

    public function setBaseCurrency(?string $base_currency): self
    {
        $this->base_currency = $base_currency;

        return $this;
    }

    public function getLotSize(): ?int
    {
        return $this->lot_size;
    }

    public function setLotSize(int $lot_size): self
    {
        $this->lot_size = $lot_size;

        return $this;
    }

    public function getTypeAssets(): ?int
    {
        return $this->type_assets;
    }

    public function setTypeAssets(int $type_assets): self
    {
        $this->type_assets = $type_assets;

        return $this;
    }
}
