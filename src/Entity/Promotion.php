<?php

namespace App\Entity;

use App\EntityTrait\JsonSerializeAssetTrait;
use App\Repository\PromotionRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=PromotionRepository::class)
 */
class Promotion extends Asset implements \JsonSerializable
{
    use JsonSerializeAssetTrait;

    /**
     * @ORM\ManyToOne(targetEntity=Issuer::class, inversedBy="asset")
     * @ORM\JoinColumn(nullable=false)
     */
    private $issuer;

    /**
     * @ORM\ManyToOne(targetEntity=Exchange::class, inversedBy="asset")
     * @ORM\JoinColumn(nullable=false)
     */
    private $exchange;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIssuer(): ?Issuer
    {
        return $this->issuer;
    }

    public function setIssuer($issuer): self
    {
        $this->issuer = $issuer;

        return $this;
    }

    public function getExchange(): ?Exchange
    {
        return $this->exchange;
    }

    public function setExchange($exchange): self
    {
        $this->exchange = $exchange;

        return $this;
    }
}
