<?php

namespace App\Entity;

use App\Repository\BondRepository;
use Doctrine\ORM\Mapping as ORM;
use App\EntityTrait\JsonSerializeAssetTrait;

/**
 * @ORM\Entity(repositoryClass=BondRepository::class)
 */
class Bond extends Asset implements \JsonSerializable
{
    use JsonSerializeAssetTrait;
    /**
     * @ORM\ManyToOne(targetEntity=Issuer::class, inversedBy="bonds")
     *  @ORM\JoinColumn(nullable=false)
     */
    private $issuer;

    /**
     * @ORM\ManyToOne(targetEntity=Exchange::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $exchange;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIssuer(): ?Issuer
    {
        return $this->issuer;
    }

    public function setIssuer(?Issuer $issuer): self
    {
        $this->issuer = $issuer;

        return $this;
    }

    public function getExchange(): ?Exchange
    {
        return $this->exchange;
    }

    public function setExchange(?Exchange $exchange): self
    {
        $this->exchange = $exchange;

        return $this;
    }
}
