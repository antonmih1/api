<?php

namespace App\Controller;

use App\Entity\Bond;
use App\Entity\Issuer;
use App\Entity\Promotion;
use App\Repository\IssuerRepository;
use App\Service\IssuerService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Helper\ResponseHelper;

/**
 * @package App\Controller
 * @Route("/api_issuer", name="api_issuer")
 */
class IssuerController extends AbstractController
{
    private $issuerService;
    private $responseHelper;

    public function __construct(IssuerService $issuerService, ResponseHelper $responseHelper)
    {
        $this->issuerService = $issuerService;
        $this->responseHelper = $responseHelper;
    }

    /**
     * @param IssuerRepository $issuerRepository
     * @return JsonResponse
     * @Route("/issuers", name="issuers", methods={"GET"})
     */
    public function getIssuer(IssuerRepository $issuerRepository)
    {
        $issuer = $issuerRepository->findAll();
        if (!$issuer){
            $data = [
                'status' => 404,
                'errors' => "Issuer not found",
            ];
            return $this->responseHelper->response($data, 404);
        }
        return $this->responseHelper->response($issuer);
    }

    /**
     * @Route("/issuer/{id}", name="issuer_id", methods={"GET"})
     * @param Issuer $issuer
     * @return Response
     */
    public function getIssuerId(Issuer $issuer)
    {
        $assets = [];
        if ($issuer) {
            $promotionRepository = $this->getDoctrine()
                ->getRepository(Promotion::class);
            $bondRepository = $this->getDoctrine()
                ->getRepository(Bond::class);
            $assets = $this->issuerService->getIssuerId($promotionRepository, $bondRepository, $issuer);
        }

        return $this->responseHelper->response($assets);
    }

}
