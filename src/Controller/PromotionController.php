<?php

namespace App\Controller;

use App\Entity\Promotion;
use App\Helper\ResponseHelper;
use Doctrine\ORM\EntityManagerInterface;
use App\Repository\PromotionRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @package App\Controller
 * @Route("/api_promotion", name="post_api")
 */
class PromotionController extends AbstractController
{
    private $responseHelper;

    public function __construct(ResponseHelper $responseHelper)
    {
        $this->responseHelper = $responseHelper;
    }

    /**
     * @param PromotionRepository $promotionRepository
     * @return JsonResponse
     * @Route("/promotions", name="promotions", methods={"GET"})
     */
    public function getPromotions(PromotionRepository $promotionRepository)
    {
        $promotion = $promotionRepository->findAll();
        if (!$promotion){
            $data = [
                'status' => 404,
                'errors' => "Promotion not found",
            ];
            return $this->responseHelper->response($data, 404);
        }
        return $this->responseHelper->response($promotion);
    }

    /**
     * @Route("/promotion/id/{id}", name="promotion_id", methods={"GET"})
     * @param PromotionRepository $promotionRepository
     * @param $id
     * @return Response
     */
    public function getPromotionId(PromotionRepository $promotionRepository, $id)
    {
        $promotion = $promotionRepository->find($id);
        if (!$promotion){
            $data = [
                'status' => 404,
                'errors' => "Promotion not found",
            ];
            return $this->responseHelper->response($data, 404);
        }
        return $this->responseHelper->response($promotion);
    }

    /**
     * @Route("/promotion/isin/{isin}", name="promotion_isin", methods={"GET"})
     * @param PromotionRepository $promotionRepository
     * @param $isin
     * @return Response
     */
    public function getPromotionIsin(PromotionRepository $promotionRepository, $isin)
    {
        $promotion = $promotionRepository->findOneBy(['isin' => $isin]);
        if (!$promotion){
            $data = [
                'status' => 404,
                'errors' => "Promotion not found",
            ];
            return $this->responseHelper->response($data, 404);
        }
        return $this->responseHelper->response($promotion);
    }

}
