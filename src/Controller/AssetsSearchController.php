<?php

namespace App\Controller;

use App\Entity\Bond;
use App\Entity\Promotion;
use App\Helper\ResponseHelper;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\BondRepository;
use App\Repository\PromotionRepository;
use App\Service\AssetService;

/**
 * @package App\Controller
 * @Route("/assets", name="assets")
 */
class AssetsSearchController extends AbstractController
{

    private $responseHelper;
    private $assetsService;

    public function __construct(ResponseHelper $responseHelper, AssetService $assetService)
    {
        $this->responseHelper = $responseHelper;
        $this->assetsService = $assetService;
    }

    /**
     * @Route("/search", name="issuers_assets", methods={"GET"})
     * @return Response
     */
    public function search(Request $request)
    {
        $assets = [];
        if ($query = $request->query->get('query')) {
            if (iconv_strlen($query) >= 3) {
                $type = $request->query->get('type');
                $promotionRepository = $this->getDoctrine()
                    ->getRepository(Promotion::class);
                $bondRepository = $this->getDoctrine()
                    ->getRepository(Bond::class);
                $assets = $this->assetsService->search($promotionRepository, $bondRepository, $query, $type);
            } else {
                $data = [
                    'status' => 404,
                    'errors' => "at least 3 characters",
                ];
                return $this->responseHelper->response($data);
            }
        }
        return $this->responseHelper->response($assets);
    }
}
