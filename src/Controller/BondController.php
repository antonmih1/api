<?php

namespace App\Controller;

use App\Entity\Bond;
use App\Helper\ResponseHelper;
use App\Repository\BondRepository;
use Doctrine\ORM\EntityManagerInterface;
use App\Repository\PromotionRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @package App\Controller
 * @Route("/api_bond", name="api_bond")
 */
class BondController extends AbstractController
{
    private $responseHelper;

    public function __construct(ResponseHelper $responseHelper)
    {
        $this->responseHelper = $responseHelper;
    }

    /**
     * @param BondRepository $bondRepository
     * @return JsonResponse
     * @Route("/bonds", name="bonds", methods={"GET"})
     */
    public function getBonds(BondRepository $bondRepository)
    {
        $bonds = $bondRepository->findAll();
        if (!$bonds){
            $data = [
                'status' => 404,
                'errors' => "Bonds not found",
            ];
            return $this->responseHelper->response($data, 404);
        }
        return $this->responseHelper->response($bonds);
    }

    /**
     * @Route("/bond/id/{id}", name="bond_id", methods={"GET"})
     * @param BondRepository $bondRepository
     * @param $id
     * @return Response
     */
    public function getBondId(BondRepository $bondRepository, $id)
    {
        $bond = $bondRepository->find($id);
        if (!$bond){
            $data = [
                'status' => 404,
                'errors' => "Bond not found",
            ];
            return $this->responseHelper->response($data, 404);
        }
        return $this->responseHelper->response($bond);
    }

    /**
     * @Route("/bond/isin/{isin}", name="bond_isin", methods={"GET"})
     * @param BondRepository $bondRepository
     * @param $isin
     * @return Response
     */
    public function getBondIsin(BondRepository $bondRepository, $isin)
    {
        $bond = $bondRepository->findOneBy(['isin' => $isin]);
        if (!$bond){
            $data = [
                'status' => 404,
                'errors' => "Bond not found",
            ];
            return $this->responseHelper->response($data, 404);
        }
        return $this->responseHelper->response($bond);
    }

}
