<?php

namespace App\RepositoryTrait;

use App\Entity\Issuer;
use App\Entity\Promotion;

/**
 * Trait class
 */
trait GetAssetsTrait
{
    /**
     * @param Issuer $issuer
     * @return mixed
     */
    public function getAssetsThisIssuer($issuer)
    {
        $qb = $this->createQueryBuilder('t')
            ->orderBy('t.date_start', 'ASC');

        $qb = $qb
            ->andWhere('t.issuer = :issuer')
            ->andWhere('t.date_end IS NULL')
            ->setParameter('issuer', $issuer);

        return $qb->getQuery()->getResult();
    }

    /**
     * @param $query
     * @param null $type
     * @return mixed
     */
    public function getAssets($query, $type = null)
    {
        $qb = $this->createQueryBuilder('a')
            ->orderBy('a.date_start', 'ASC');

        $qb = $qb
            ->orWhere('a.name LIKE :query')
            ->orWhere('a.isin LIKE :query')
            ->orWhere('a.ticker LIKE :query')
            ->setParameter('query', '%' . $query . '%');

        if ($type) {
            $qb = $qb
                ->andWhere('a.type = :type')
                ->setParameter('type', $type);
        }

        return $qb->getQuery()->getResult();

    }
}